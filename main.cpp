#include <iostream>
#include "sort.hpp"

template<typename T>
void print_vector(std::vector<T> &vec){
  std::cout <<'(';

  size_t i;
  for (i=0; i<vec.size()-1; i++){
    std::cout<< vec[i] <<" ,";
  }
  std::cout << vec[i] << ')' << std::endl;
}
int main(){
  std::string values;
  std::vector<std::string> sort_values;
  std::cout << "Enter numbers (end to stop): "<< std::endl;

  while (values != "end"){
    std::getline(std::cin, values);
    sort_values.push_back(values);
  }
    sel_sort(sort_values);

    std::cout<<"Sorted values: ";
    print_vector(sort_values);
}
