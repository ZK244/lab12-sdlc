#include <vector>

template <typename T>
void sel_sort(std::vector<T> &list){
  size_t min;
  for (size_t i = 0; i< list.size(); i++){
    min = i;

    for (size_t j=i ; j< list.size(); j++){
      if (list[j] < list[min]){
	min=j;
      }
    }
    std::swap(list[i], list[min]);
  } 
}
