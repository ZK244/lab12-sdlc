#include "catch.hpp"
#include "sort.hpp"

template<typename T>
bool is_sorted(std::vector<T> input) {
  if (input.size() <= 1)
    return true;

  for (size_t i = 1; i < input.size(); ++i)
    if (input[i-1] > input[i])
      return false;
  return true;
}

TEST_CASE("selection sort works as expected on vectors of floats", "[selection_sort]") {
  std::vector<float> numbers = {4.23, 5.1, 7.324, 1.876, 1.43, 0, -2.324, 8.6675};
  
  REQUIRE_FALSE(is_sorted(numbers));

  selection_sort(numbers);
  REQUIRE(is_sorted(numbers));
}

TEST_CASE("selection sort works as expected on vectors of chars", "[selection_sort]") {
  std::vector<char> numbers = {'A', 'D', 'G', 'c', 'S', 'M'};
  
  REQUIRE_FALSE(is_sorted(numbers));

  selection_sort(numbers);
  REQUIRE(is_sorted(numbers));
}

TEST_CASE("selection sort works as expected on vectors of ints", "[selection_sort]") {
  std::vector<int> numbers = {3, 5, 8, 2, 1, 0, -4, 9};
  
  REQUIRE_FALSE(is_sorted(numbers));

  selection_sort(numbers);
  REQUIRE(is_sorted(numbers));
}

TEST_CASE("calling selection sort on a sorted vector results in equal vector", "[selection_sort]") {
  std::vector<int> numbers = {-7, -4, -1, 0, 2, 5, 7, 9, 12};
  std::vector<int> input = numbers;

  REQUIRE_THAT(input, Catch::Matchers::Equals(numbers));
  
  selection_sort(input);
  REQUIRE_THAT(input, Catch::Matchers::Equals(numbers));
}

TEST_CASE("calling selection sort on a vector filled with the same element results in an equal vector", "[selection_sort]") {
  std::vector<int> numbers = {-4, -4, -4, -4};
  std::vector<int> input = numbers;

  REQUIRE_THAT(input, Catch::Matchers::Equals(numbers));
  
  selection_sort(input);
  REQUIRE_THAT(input, Catch::Matchers::Equals(numbers));
}


TEST_CASE("calling selection sort on an empty vector results in an equal empty vector", "[selection_sort]") {
  std::vector<int> numbers = {};
  std::vector<int> input = numbers;

  REQUIRE_THAT(input, Catch::Matchers::Equals(numbers));
  
  selection_sort(input);
  REQUIRE_THAT(input, Catch::Matchers::Equals(numbers));
}
