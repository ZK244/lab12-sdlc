CXX=g++
CFLAGS=-Wall

main:
	$(CXX) main.cpp $(CFLAGS) -o main
	./main

test: test_main.o
	$(CXX) test_main.o test_sort.cpp $(CFLAGS) -o sort_test
	./sort_test

test_main.o: test_main.cpp
	$(CXX) test_main.cpp $(CFLAGS) -c -o test_main.o

clean:
	rm -f *.o main sort_test

.PHONY: clean test main
